import { FaTrashAlt } from 'react-icons/fa';

const LineProduct = ({ product, handleCheck, handleDelete }) => {
  return (
    <li className="item">
      <input type="checkbox" onChange={() => handleCheck(product.id)} checked={product.checked} />

      <label style={(product.checked) ? {textDecoration: 'line-through'} : null}
        onDoubleClick={() => handleCheck(product.id)}>{product.item}</label>

      <FaTrashAlt onClick={() => handleDelete(product.id)} role="button" tabIndex="0" aria-label={`Delete ${product.item}`} />
    </li>
  )
}

export default LineProduct;