import Header from './Header';
import Nav from './Nav';
import Footer from './Footer';
import About from './About';
import Product from './Product';
import { Route, Routes } from 'react-router-dom';

function App() {

  return (
    <div className="App">
      <Header title="Tokopedia" />
      <Nav />
      <Routes>
        <Route path="/" element={<Product />} />
        <Route path="/about" element={<About />} />
      </Routes>
      <Footer className="Footer" />
    </div>
  );
}

export default App;
