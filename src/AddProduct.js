import { FaPlus } from 'react-icons/fa';
import { useRef } from 'react';

const AddProduct = ({ newProduct, setNewProduct, handleSubmit }) => {

  const inputRef = useRef();

  return (
    <form className="addForm" onSubmit={handleSubmit}>
      <label htmlFor="addItem">Add Item</label>
      <input 
        autoFocus 
        ref = {inputRef}
        id="addItem" 
        type="text" 
        placeholder="Add Item" 
        required 
        value={newProduct} 
        onChange={(e) => setNewProduct(e.target.value)} />
      <button type="submit" onClick={() => inputRef.current.focus()}>
        <FaPlus />
      </button>
    </form>
  )
}

export default AddProduct;