import ProductList from './ProductList';


const Content = ({ products, handleCheck, handleDelete }) => {

  return (
    <>
      {products.length ? (
        <ProductList 
          products = { products } 
          handleCheck = { handleCheck }
          handleDelete = { handleDelete } 
        />
      ) : (
        <p style={{marginTop: '2rem' }}>Your list is empty.</p>
      )}
    </>
  )
}

export default Content;