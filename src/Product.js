import SearchProduct from './SearchProduct';
import AddProduct from './AddProduct';
import Content from './Content';
import { useState, useEffect } from 'react';
import Api from './Api';

const Product = () => {

  // const [products, setProducts] = useState([
  //   {id: 1, checked: false, item: 'Samsung Galaxy 10'},
  //   {id: 2, checked: false, item: 'Iphone 12'},
  //   {id: 3, checked: false, item: 'Oppo'}
  // ]);
  const API_URL = 'http://localhost:3500/products';

  const [products, setProducts] = useState([]);
  const [newProduct, setNewProduct] = useState('');
  const [search, setSearch] = useState('');
  const [fetchError, setFetchError] = useState(null);
  const [isLoading, setIsLoading] = useState(true);


  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const response = await fetch(API_URL);
        if (!response.ok) {
          throw Error('Tidak dapat menerima data');
        }
        const listProducts = await response.json();
        // console.log(listProducts);
        setProducts(listProducts);
        setFetchError(null);
      } catch (err) {
        // console.log(err.message);
        setFetchError(err.message);
      } finally {
        setIsLoading(false);
      }
    }

    setTimeout(() => {
      (async () => await fetchProducts())();
    }, 2000);

  }, []);


  const addProduct = async (item) => {
    const id = products.length ? products[products.length - 1].id + 1 : 1;
    const newItem = {id: id, checked: false, item};
    const listProduct = [...products, newItem];

    setProducts(listProduct);

    const method = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newItem)
    };

    const result = await Api(API_URL, method);
    if (result) {
      setFetchError(result);
    }
  }

  const handleCheck = async (id) => {
    // console.log(`key: ${id}`);
    const listProduct = products.map((product) => {
      return product.id === id ? {...product, checked: !product.checked} : product;
    });
    setProducts(listProduct);

    const myItem = listProduct.filter(product => product.id === id);
    const method = {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ checked: myItem[0].checked })
    };

    const result = await Api(`${API_URL}/${id}`, method);
    if (result) {
      setFetchError(result);
    }
  }

  const handleDelete = async (id) => {
    // console.log(id);
    const listProduct = products.filter(product => {
      return product.id !== id
    });
    setProducts(listProduct);

    const method = {method: 'DELETE'};

    const result = await Api(`${API_URL}/${id}`, method);
    if (result) {
      setFetchError(result);
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log('submitted');
    if (!newProduct) {
      return;
    }
    addProduct(newProduct);
    // add product
    setNewProduct('');
  }

  return (
    <div className="App">
      <AddProduct newProduct={ newProduct } setNewProduct={ setNewProduct } handleSubmit = { handleSubmit } />
      <SearchProduct search={search} setSearch={setSearch} />
      <main>
        {isLoading && <p>Loading data...</p>}
        {fetchError && <p style={{ color: "red" }}>{`Error: ${fetchError}`}</p>}
        {!fetchError && !isLoading &&
          <Content 
            products={ products.filter(product => ((product.item).toLowerCase()).includes(search.toLowerCase())) } 
            handleCheck={ handleCheck } 
            handleDelete={ handleDelete } 
          />
        }
      </main>
    </div>
  )
}

export default Product;