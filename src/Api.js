const Api = async (url = '', method = null, errMessage = null) => {

  try {
    const response = await fetch(url, method);
    if (!response.ok) {
      throw Error('Please reload the app');
    }

  } catch(err) {
    errMessage = err.message;
  } finally {
    return errMessage;
  }
}

export default Api;